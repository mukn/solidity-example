const Web3 = require('web3');
let source = require("../contracts/ClosingMin.json");
const keys = require('../secret/keys');
const Transaction = require('ethereumjs-tx');
const ethUtils = require("ethereumjs-util");

let contracts = source["contracts"];
const bytecode = contracts["ClosingMin.sol:ClosingMin"].bin;
let abi = JSON.parse(contracts["ClosingMin.sol:ClosingMin"].abi);

/*
   -- Define Provider & Variables --
*/
function instantiateWeb3(infuraProjectID) {
    var web3 = new Web3(new Web3.providers.HttpProvider(`https://ropsten.infura.io/v3/${infuraProjectID}`));
    return web3;
}

function instantiateWeb3WSS(infuraProjectID) {
    var web3 = new Web3(new Web3.providers.WebsocketProvider(`wss://ropsten.infura.io/ws/v3/${infuraProjectID}`));
    return web3;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
 
function sign(hashedMessage, privateKey) {
    if (typeof hashedMessage === "string" && hashedMessage.slice(0, 2) === "0x") {
        hashedMessage = Buffer.alloc(32, hashedMessage.slice(2), "hex");
    }

    const sig = ethUtils.ecsign(hashedMessage, privateKey);
    return  {
        r: sig.r.toString("hex"),
        s: sig.s.toString("hex"),
        v: sig.v
    }        
}

function hash(messsage) {
    if (typeof messsage === "string") {
        return ethUtils.keccak(messsage);
    }
}

function hashAndsign(messsage, privateKey){
    let hashed = hash(messsage);
    let vrsh = sign(hashed, privateKey);

    vrsh["hash"] = hashed.toString('hex');
    return vrsh;
}

async function closingMinMethod(web3, contractAddress, senderAddress, senderPrivateKey, vrs={}) {
    let contract = new web3.eth.Contract(abi, contractAddress);
    let data;
    if(vrs.r === undefined) {
        data = contract.methods.timeout().encodeABI();
    } else {
        data = contract.methods.sign(vrs.v, '0x'+vrs.r, '0x'+vrs.s).encodeABI();
    }
   
    let cnt = await web3.eth.getTransactionCount(senderAddress);
    let rawTx = {
        nonce: web3.utils.toHex(cnt),
        data: data,
        gasLimit: web3.utils.toHex(800000),
        gasPrice:  web3.utils.toHex(web3.utils.toWei('20', 'gwei')),
        to: contractAddress,
        from: senderAddress
    };

    const tx = new Transaction(rawTx);
    tx.sign(Buffer.from(senderPrivateKey, 'hex'));
    let serializedTx = '0x' + tx.serialize().toString('hex');
    await web3.eth.sendSignedTransaction(serializedTx);
}

async function getBlockNumber(web3){
    return await web3.eth.getBlockNumber();
}

async function waitBlockForEvent(web3, web3wss, startBlock, endBlock, contractAddress, senderAddress, senderPrivateKey) {
    let contract = new web3wss.eth.Contract(abi, contractAddress);
    while (true) {
        let block = await web3.eth.getBlockNumber();
        if(block >= endBlock) {
            //call timeout
            await closingMinMethod(contractAddress, web3, senderAddress, senderPrivateKey)
            break;
        }
        let event = await contract.getPastEvents('SignaturePublished', {fromBlock: startBlock, toBlock: 'latest'});
        if(event.length === 0) {
            await sleep(4000);
            startBlock = block;
            continue;
        } else {
            //confirm event
            console.log(event);
            break;
        }
    }
}


async function isBalanced(web3, expectedBalance, contractAddress) {
    let wei = await web3.eth.getBalance(contractAddress);
    return web3.utils.toWei(expectedBalance, 'ether') === wei;
} 

 /**
   -- Deploy Contract --
*/
const deploy = async (web3, value, initializationArgument, creator, privateKey) => {
    console.log(`Attempting to deploy from account ${creator}`);

    // Create Contract Instance
    const closingMin = new web3.eth.Contract(abi);


    // Create Constructor Tx
    const closingMinTx = closingMin.deploy({
        data: bytecode,
        arguments: initializationArgument
    });

    let rawTx = {
        nonce: web3.utils.toHex(await web3.eth.getTransactionCount(creator)),
        data: closingMinTx.encodeABI(),
        gas: (await closingMinTx.estimateGas()),
        gasPrice:  web3.utils.toHex(web3.utils.toWei('20', 'gwei')),
        chainId: '0x03',
        value: web3.utils.toHex(web3.utils.toWei(value, 'ether'))
    };

    // Sign Transacation and Send
    const tx = new Transaction(rawTx);
    tx.sign(privateKey);
    var serializedTx = '0x' + tx.serialize().toString('hex');


    // Send Tx and Wait for Receipt
    const createReceipt = await web3.eth.sendSignedTransaction(serializedTx);
   
    console.log(
        `Contract deployed at address: ${createReceipt.contractAddress}`
    );
    return createReceipt.contractAddress;
};


module.exports = {
   deploy,
   isBalanced,
   waitBlockForEvent,
   getBlockNumber,
   closingMinMethod,
   hashAndsign,
   instantiateWeb3WSS,
   instantiateWeb3
}