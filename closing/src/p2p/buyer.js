'use strict'

const path = require('path')
const execa = require('execa')
const pDefer = require('p-defer')
const uint8ArrayToString = require('uint8arrays/to-string')
const buyerInteractions  = require('../interactions/buyer.js');
const keys = require('../secret/keys.js');

function startProcess(name) {
    return execa('node', [path.join(__dirname, name)], {
        cwd: path.resolve(__dirname),
        all: true
    })
}

function getArgument(parsed, address) {
    let rtn = [];
    if(!parsed.deadline) {
        throw new Error("Expected Deadline");
    } else {
        rtn.push(parseInt(parsed.deadline));
    }

    rtn.push(address);

    if(!parsed.sellerAddress) {
        throw new Error("Expected Seller's address");
    } else {
        rtn.push(parsed.sellerAddress);
    }
    
    if(!parsed.digest) {
        throw new Error("Expected Digest");
    } else {
        rtn.push("0x"+parsed.digest);
    }

    return rtn;
}

async function closing () {
    const message = 'Start messaging';
    let dialerOutput = '';

    let messageSent = false;
    const dialerReady = pDefer();
    const messageReceived = pDefer();

    process.stdout.write('==================================================================\n');

    // Step 2 process  [Buyer]
    process.stdout.write(message + '\n');
    process.stdout.write('node dialer.js\n');
    const dialerProc = startProcess('dialer.js');
    dialerProc.all.on('data', async (data) => {
        dialerOutput = uint8ArrayToString(data);
        process.stdout.write(data);
        
        if(!messageSent && dialerOutput.includes('Type a message and see what happens')) {
            let obj = buyerInteractions.order();
            let myJSON = JSON.stringify(obj);
            dialerProc.stdin.write(myJSON);
            messageSent = false;
        }

        if (!messageSent && dialerOutput.includes('{')) {
            try {
                let parsed = JSON.parse(dialerOutput.substr(1));
                switch(parsed["topic"]){
                    case "Offer":
                        let sellerTotalCost = parsed.totalCost;
                        buyerInteractions.expectedOrderCost(sellerTotalCost);
                        let web3 = buyerInteractions.instantiateWeb3(keys.infuraProjectID);
                        let constructorArgument = getArgument(parsed, keys.BuyerAddress);
                        let contractAddress = await buyerInteractions.deployClosingMinContract(web3, sellerTotalCost.toString(), constructorArgument, keys.BuyerAddress, keys.getBuyerPrivateKey());
                        let [deadline, , , digest] = constructorArgument;
                        let buyerTxId = parsed.buyerTxId;
                        let sellerTxId = parsed.sellerTxId;
                        let agreementObj = buyerInteractions.agreement(digest, deadline, buyerTxId, sellerTxId, contractAddress)
                        let myJSON = JSON.stringify(agreementObj);
                        dialerProc.stdin.write(myJSON);
                        let web3WSS = buyerInteractions.instantiateWeb3WSS(keys.infuraProjectID);
                                
                        buyerInteractions.monitorChain(web3, web3WSS, deadline, contractAddress, keys.BuyerAddress, keys.getBuyerPrivateKey());
                        messageReceived.resolve();
                        break;
                    default:
                        messageReceived.resolve();
                }
            } catch(err) {
                console.error("Buyer  " + err);
                messageReceived.resolve();
            }
            dialerReady.resolve();
            messageSent = true;
        }
    })

    await dialerReady.promise;
    process.stdout.write('==================================================================\n');
    await messageReceived.promise;
    process.stdout.write('Closing chat channel\n')
    dialerProc.kill();
    await Promise.all([
        dialerProc
    ]).catch((err) => {
        if (err.signal !== 'SIGTERM') {
            throw err;
        }
    })
}

closing()
module.exports = {closing}
