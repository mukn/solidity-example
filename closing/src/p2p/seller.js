'use strict'

const path = require('path')
const execa = require('execa')
const pDefer = require('p-defer')
const uint8ArrayToString = require('uint8arrays/to-string')
const sellerInteractions = require('../interactions/seller.js');
const keys = require('../secret/keys.js');

function startProcess(name) {
    return execa('node', [path.join(__dirname, name)], {
        cwd: path.resolve(__dirname),
        all: true
    })
}


async function closing () {
    const message = 'Start messaging';
    let listenerOutput = '';

    let isListening = false;
    const listenerReady = pDefer();
    const messageReceived = pDefer();

    // Step 1 process [Seller]
    process.stdout.write('node listener.js\n')
    const listenerProc = startProcess('listener.js')
    listenerProc.all.on('data', async (data) => {
        listenerOutput = uint8ArrayToString(data);
        process.stdout.write(data);

        listenerOutput = uint8ArrayToString(data);

        if(!isListening && listenerOutput.includes('Listener ready, listening on')) {
            listenerReady.resolve();
            isListening = true;
        } else if(isListening && listenerOutput.includes('{')) {
            try {
                let parsed = JSON.parse(listenerOutput.substr(1));
                switch(parsed["topic"]){
                    case "Order":
                        let offerObj = sellerInteractions.offer(parsed, keys.SellerAddress, keys.getSellerPrivateKey());
                        let myJSON = JSON.stringify(offerObj);
                        listenerProc.stdin.write(myJSON); 
                        break;
                    case "Agreement":
                        let contractAddress = parsed.contractAddress;
                        let buyerTxId = parsed.buyerTxId;
                        let web3 = sellerInteractions.instantiateWeb3(keys.infuraProjectID);
                        await sellerInteractions.expectedContractBalance(web3, buyerTxId, contractAddress);
                        
                        await sellerInteractions.closingMinGetPaid(web3, buyerTxId, contractAddress, keys.SellerAddress, keys.getSellerPrivateKey());
                        console.log("Signing progress.....");
                        messageReceived.resolve();
                }
            } catch(err) {
                console.error("Seller  " + err);
            }
        }
    });

    await listenerReady.promise;
    process.stdout.write('==================================================================\n');

    
    await messageReceived.promise;
    process.stdout.write('Seller closing channel\n');
    listenerProc.kill();
    await Promise.all([
        listenerProc,
    ]).catch((err) => {
        if(err.signal !== 'SIGTERM') {
            throw err;
        }
    })
}

closing()
module.exports = {closing}
