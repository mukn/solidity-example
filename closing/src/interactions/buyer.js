let network = require('../network/network.js');


function instantiateWeb3(networkId){
    return network.instantiateWeb3(networkId);
}

function instantiateWeb3WSS(networkId){
    return network.instantiateWeb3WSS(networkId);
}

async function closingMinTimeout(web3, contractAddress,  address, privateKey ){
    await network.closingMinMethod(web3, contractAddress, address, Buffer.from(privateKey, 'hex'))
}

async function deployClosingMinContract(web3, amount,  constructorArgument, address, privateKey){
    return await network.deploy(web3, amount,  constructorArgument, address, Buffer.from(privateKey, 'hex'));
}

async function monitorChain(web3, web3wss, deadline, contractAddress, address, privateKey) {   
    let startBlock = await network.getBlockNumber(web3);
    let endBLock = startBlock + parseInt(deadline);
    await network.waitBlockForEvent(web3, web3wss, startBlock, endBLock, contractAddress, address, Buffer.from(privateKey, 'hex'));
}

function order() {
    //item, array is price and quantity
    let order = {
        topic: "Order",
        tesla_V: [0.1, 1],
        bugati_M: [0.1, 1],
        mukN_T_shirt: [0.0001, 10],
        totalCost : 0.201
    }
    return order;
}

function agreement(digest, deadline, buyerTxId, sellerTxId, contractAddress) {
    let agreement = {
        topic: "Agreement",
        totalCost : 0.201,
        digest: digest,
        deadline: deadline,
        contractAddress: contractAddress,
        buyerTxId: buyerTxId,
        sellerTxId: sellerTxId

    }
    return agreement;
}

function expectedOrderCost(sellerTotalCost){
    let totalCost = order().totalCost;
    if(totalCost !== sellerTotalCost) {
        throw new Error("The Order total cost and Seller's total cost should be equal" + totalCost + " " + sellerTotalCost);
    }
}

module.exports = {
    expectedOrderCost,
    agreement,
    order,
    monitorChain,
    deployClosingMinContract,
    closingMinTimeout,
    instantiateWeb3WSS,
    instantiateWeb3
}
