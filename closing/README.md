## Closing transaction between buyer and seller on-chain

Make sure you have nodejs installed
Install packages by running the below:
`npn install`

Open `nano src/secret/keys.js`
Edit private keys, address, and infura ID.

You can start running `./startClosingMin.sh`.
This would create two processes and start seller p2p and buyer p2p

You can also running by running seller first `./seller.sh` and followed by `./buyer.sh`


